package com.affymetrix.genometryImpl.event;

public interface PropertyListener {
	public void propertyDisplayed(int prop_displayed);
}
