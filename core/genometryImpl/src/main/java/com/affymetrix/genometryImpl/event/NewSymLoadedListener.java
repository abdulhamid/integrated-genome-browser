package com.affymetrix.genometryImpl.event;

/**
 *
 * @author hiralv
 */
public interface NewSymLoadedListener {
	public void newSymLoaded(NewSymLoadedEvent e);
}
