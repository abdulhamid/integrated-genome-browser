package com.affymetrix.genometryImpl.event;

public interface GenericActionDoneCallback {
	public void actionDone(GenericAction action);
}
