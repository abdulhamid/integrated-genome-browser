package com.affymetrix.genometryImpl.quickload;

public interface QuickLoadSymLoaderHook {
	public QuickLoadSymLoader processQuickLoadSymLoader(QuickLoadSymLoader quickLoadSymLoader);
}
