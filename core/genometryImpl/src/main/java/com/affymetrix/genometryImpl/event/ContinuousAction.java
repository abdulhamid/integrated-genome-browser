package com.affymetrix.genometryImpl.event;

import java.awt.event.ActionListener;

/**
 *
 * @author hiralv
 */
public interface ContinuousAction extends ActionListener{
	//Should use delay over here. But OK for now.
}
