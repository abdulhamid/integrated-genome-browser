 ___  __    _  _______  _______  _______  ______    _______  _______  _______  ______  
|   ||  |  | ||       ||       ||       ||    _ |  |   _   ||       ||       ||      | 
|   ||   |_| ||_     _||    ___||    ___||   | ||  |  |_|  ||_     _||    ___||  _    |
|   ||       |  |   |  |   |___ |   | __ |   |_||_ |       |  |   |  |   |___ | | |   |
|   ||  _    |  |   |  |    ___||   ||  ||    __  ||       |  |   |  |    ___|| |_|   |
|   || | |   |  |   |  |   |___ |   |_| ||   |  | ||   _   |  |   |  |   |___ |       |
|___||_|  |__|  |___|  |_______||_______||___|  |_||__| |__|  |___|  |_______||______| 
 _______  _______  __    _  _______  __   __  _______                                  
|       ||       ||  |  | ||       ||  |_|  ||       |                                 
|    ___||    ___||   |_| ||   _   ||       ||    ___|                                 
|   | __ |   |___ |       ||  | |  ||       ||   |___                                  
|   ||  ||    ___||  _    ||  |_|  ||       ||    ___|                                 
|   |_| ||   |___ | | |   ||       || ||_|| ||   |___                                  
|_______||_______||_|  |__||_______||_|   |_||_______|                                 
 _______  ______    _______  _     _  _______  _______  ______                         
|  _    ||    _ |  |       || | _ | ||       ||       ||    _ |                        
| |_|   ||   | ||  |   _   || || || ||  _____||    ___||   | ||                        
|       ||   |_||_ |  | |  ||       || |_____ |   |___ |   |_||_                       
|  _   | |    __  ||  |_|  ||       ||_____  ||    ___||    __  |                      
| |_|   ||   |  | ||       ||   _   | _____| ||   |___ |   |  | |                      
|_______||___|  |_||_______||__| |__||_______||_______||___|  |_|    

The Integrated Genome Browser (IGB, pronounced ig-bee) is an application 
intended for visualization and exploration of genomes and corresponding 
annotations from multiple data sources.

Research and development of IGB was supported in part by NIH grant R01HG003040
(PI Gregg Helt) and NSF grant 0820371 (PI Ann Loraine).


----------------- DOCUMENTATION ----------------------------

An IGB user's guide is available on-line here:

https://wiki.transvar.org/confluence/display/igbman/Home

More Information Available here: 

http://bioviz.org